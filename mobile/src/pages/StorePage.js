import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Footer from '../components/Footer';

import axios from 'axios';

import { API_SERVICE } from '../config/URI';

const StorePage = () => {

    const [data, setData] = useState([])

    const [openA, setOpenA] = React.useState(false);

    const handleClickOpenA = () => {
        setOpenA(true);
    };

    const handleCloseA = () => {
        setOpenA(false);
    };

    useEffect(() => {
      axios.get(`${API_SERVICE}/api/v1/main/getallprojects`).then(res => {
        setData(res.data.data);
      })
    }, [])

    const [r_id, setR_id] = useState('')

    const handleClickConfirm = (id) => {
      setR_id(id)
      handleClickOpenA()
    }

    const resetEntries = () => {
      axios.get(`${API_SERVICE}/api/v1/main/delmobileproductbyid/${r_id}`).then(res => {
        console.log('deleted');
      }).catch(err => {
        console.log('error');
      })
      handleCloseA()
    }

    function projects(proj){
      let date_ = new Date(proj.date).toDateString()
      return(
        <div className="card card-body rounded mt-2">
          <h4>{proj.title}</h4>
          <h6>{proj.category}</h6>
          <label className="text-muted">Assigned {date_}</label>
          <div>
            {/* <Button style={{ margin : '5px',float: 'left', backgroundColor: "rgb( 112, 48, 160)" }} href="/projects" variant="contained" color="primary">
              Submit
            </Button> */}
            <Button onClick={() => handleClickConfirm(proj._id)} style={{ margin : '5px',float: 'left', backgroundColor: "rgb( 112, 48, 160)" }} variant="contained" color="primary">
              Reset
            </Button>
            <Button href = {`/projects?id=${proj._id}`} style={{ float: 'right', backgroundColor: "rgb( 112, 48, 160)" , marginTop: "6px"}} variant="contained" color="primary">
              Start
            </Button>
          </div>
        </div>
      );
    }

    return (
        <>
            <div className="container mt-4 mb-5">
                <h2 className="fw-bold">
                    Projects
                </h2>
                <section className="mt-3">    
                    {data.length === 0?<div>Loading...</div>:data.map(projects)}
                    <br></br>
                </section>
            </div>

                <Dialog
                    open={openA}
                    onClose={handleCloseA}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Alert"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to Reset the project ?
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleCloseA} color="primary">
                        No
                    </Button>
                    <Button onClick={resetEntries} color="primary" autoFocus>
                        Yes
                    </Button>
                    </DialogActions>
                </Dialog>

            <Footer />
        </>
    )
}

export default StorePage
