import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/core/styles';
import Footer from '../components/Footer';

import queryString from 'query-string';
import axios from 'axios';

import { API_SERVICE } from '../config/URI';

const useStyles = makeStyles((theme) => ({
    appBar: {
      position: 'relative',
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    button: {
        backgroundColor: "rgb( 112, 48, 160)"
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


const ProjectPage = () => {

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [data, setData] = useState([])

    useEffect(() => {
        const { id } = queryString.parse(window.location.search);
        console.log(id);
        axios.get(`${API_SERVICE}/api/v1/main/getproject/${id}`).then(res=>{
            setData(res.data)
        })
    }, [])

    const project = (proj) => {
        return(
            <div>
                <h2 className="fw-bold">
                    {proj.title}
                </h2> 

                <Button
                    className="mt-3"
                    style={{ height: '50px' }}
                    type="button"
                    fullWidth
                    href={`/addproduct?id=${proj._id}`}
                    size="large"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                >
                    Start Capture
                </Button>
                <h2 style={{margin:"20px 0 20px 0"}}>Download files</h2>
                {proj.file1 ==="" ? <div></div>: (<a href={proj.file1}><Button fullWidth className="mt-3" type="button" variant="contained" color="secondary">Download</Button></a>)}
                {proj.file2 ==="" ? <div></div>: (<a href={proj.file2}><Button fullWidth className="mt-3" type="button" variant="contained" color="secondary">Download</Button></a>)}
                <h2 style={{margin:"20px 0 20px 0"}}>Project Description</h2>
                <p style={{margin:"20px 0 20px 0"}}>
                    {proj.content}
                </p>
                <h2 style={{margin:"20px 0 20px 0"}}>Project Note</h2>
                <p style={{margin:"20px 0 20px 0"}}>
                    {proj.notes}
                </p>
            </div>
        );
    }

    return (
        <>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                    <CloseIcon />
                    </IconButton>
                </Toolbar>
                </AppBar>
                <div className="container mt-5">
                    <center>
                        <img className="rounded shadow w-75" src="https://5.imimg.com/data5/YD/YB/ZF/SELLER-31792487/e3692f3fe8071a099727fb2e2b83b1d5-500x500.jpg" />
                        <h2 className="mt-3">
                            Milk <br />
                            <small>(1:4)</small>
                        </h2>

                        
                    </center>
                    <div class="list-group mt-3">
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>SKU</b>
                            <span className="float-end">110NM878JU100</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Price</b>
                            <span className="float-end">110$</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Expiry</b>
                            <span className="float-end">01/07/2021</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Litre</b>
                            <span className="float-end">500</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Section</b>
                            <span className="float-end">Grocery</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Shelve</b>
                            <span className="float-end">1</span> 
                        </a>
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <b>Position</b>
                            <span className="float-end">4</span> 
                        </a>
                    </div>
                </div>
            </Dialog>


            <div className="container mt-4">
                { data.length === 0?<div>Loading...</div>:data.map(project) }
            </div>
            
            <Footer />
        </>
    )
}

export default ProjectPage
