import React, {useState, useEffect} from 'react';
import BarcodeScannerComponent from "react-webcam-barcode-scanner";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Footer from '../components/Footer';

import { makeStyles } from '@material-ui/core/styles';

import Dropzone from 'react-dropzone';

import queryString from 'query-string';
import axios from 'axios';

import { firestore, storage } from '../Firebase/index';

import { v4 as uuid4 } from 'uuid';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { API_SERVICE } from '../config/URI';

const useStyles = makeStyles((theme) => ({
    button: {
        backgroundColor: "rgb( 112, 48, 160)"
    }
}));

const AddProduct = () => {

    const [file1,setFile1] = useState([])
    const [file2,setFile2] = useState([])
    const [file3,setFile3] = useState([])

    const d = new Date();

    const [prod, setProd] = useState({
        pdId: "",
        pdName: "",
        pdQty: "",
        pdPrice: "",
        pdFile1: "",
        pdFile2: "",
        pdFile3: "",
        date: d
    })

    const changeEle = (event) => {
        const {name, value} = event.target;
        setProd(prevValue => {
            return {
                ...prevValue,
                [name]: value
            }
        })
        console.log(prod);
    }

    React.useEffect(() => {
        if (file1.length > 0) {
            onSubmit1();
        } else {
            console.log("N");
        }
    }, [file1]);

    React.useEffect(() => {
        if (file2.length > 0) {
            onSubmit2();
        } else {
            console.log("N");
        }
    }, [file2]);

    React.useEffect(() => {
        if (file3.length > 0) {
            onSubmit3();
        } else {
            console.log("N");
        }
    }, [file3]);

    const handleDrop1 = async (acceptedFiles) => {
        setFile1(acceptedFiles.map(file1 => file1));
    }

    const handleDrop2 = async (acceptedFiles) => {
        setFile2(acceptedFiles.map(file2 => file2));
    }

    const handleDrop3 = async (acceptedFiles) => {
        setFile3(acceptedFiles.map(file3 => file3));
    }

    const onSubmit1 = () => {
        if (file1.length > 0) {
            file1.forEach(file => {
                const timeStamp = Date.now();
                var uniquetwoKey = uuid4();
                uniquetwoKey = uniquetwoKey + timeStamp;
                const uploadTask = storage.ref(`projects/${uniquetwoKey}/${file.name}`).put(file);
                uploadTask.on('state_changed', (snapshot) => {
                    const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    setOpenS(true);
                    setTextS(`Uploading ${progress} %`);
                },
                (error) => {
                    console.log(error)
                },
                async () => {
                    // When the Storage gets Completed
                    const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                    setTextS('File Uploaded');
                    setProd((prevVal) => ({...prevVal, "pdFile1": filePath}))
                });
            })
        } else {
            console.log('No File Selected Yet');
        }
    }

    const onSubmit2 = () => {
        if (file2.length > 0) {
            file2.forEach(file => {
                const timeStamp = Date.now();
                var uniquetwoKey = uuid4();
                uniquetwoKey = uniquetwoKey + timeStamp;
                const uploadTask = storage.ref(`projects/${uniquetwoKey}/${file.name}`).put(file);
                uploadTask.on('state_changed', (snapshot) => {
                    const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    setOpenS(true);
                    setTextS(`Uploading ${progress} %`);
                },
                (error) => {
                    console.log(error)
                },
                async () => {
                    // When the Storage gets Completed
                    const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                    setTextS('File Uploaded');
                    setProd((prevVal) => ({...prevVal, "pdFile2": filePath}))
                });
            })
        } else {
            console.log('No File Selected Yet');
        }
    }

    const onSubmit3 = () => {
        if (file3.length > 0) {
            file3.forEach(file => {
                const timeStamp = Date.now();
                var uniquetwoKey = uuid4();
                uniquetwoKey = uniquetwoKey + timeStamp;
                const uploadTask = storage.ref(`projects/${uniquetwoKey}/${file.name}`).put(file);
                uploadTask.on('state_changed', (snapshot) => {
                    const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    setOpenS(true);
                    setTextS(`Uploading ${progress} %`);
                },
                (error) => {
                    console.log(error)
                },
                async () => {
                    // When the Storage gets Completed
                    const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                    setTextS('File Uploaded');
                    setProd((prevVal) => ({...prevVal, "pdFile3": filePath}))
                });
            })
        } else {
            console.log('No File Selected Yet');
        }
    }

    const classes = useStyles();
    // const [ data, setData ] = React.useState('Not Found');

    const [data, setData] = useState([])

    useEffect(() => {
        const { id } = queryString.parse(window.location.search);
        console.log(id);
        axios.get(`${API_SERVICE}/api/v1/main/getproject/${id}`).then(res=>{
            setData(res.data)
            console.log(res.data)
        })
    }, [])

    const [openS, setOpenS] = React.useState(false);
    const [textS, setTextS] = React.useState("");

    const handleClickS = () => {
        setOpenS(true);
    };

    const handleCloseS = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenS(false);
    };    

    const comp1 = (data) => {
        if(data.shelf === true){
            return(
                <>
                <h3>Shelf Availability</h3>
                <div>
                    <Dropzone onDrop={handleDrop1}>
                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps({ className: "dropzone" })}>
                                <input {...getInputProps()} />
                                <Button fullWidth size="large" color="primary" variant="contained">Upload File </Button>
                            </div>
                        )}
                    </Dropzone>
                </div>
                </>
            );
        }
    }

    const comp2 = (data) => {
        if(data.image === true){
            return(
                <>
                <h3>Planogram</h3>
                <div>
                    <Dropzone onDrop={handleDrop2}>
                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps({ className: "dropzone" })}>
                                <input {...getInputProps()} />
                                <Button fullWidth size="large" color="primary" variant="contained">Upload Image</Button>
                            </div>
                        )}
                    </Dropzone>
                </div>
                </>
            );
        }
    }

    const comp3 = (data) => {
        if(data.inventory === true){
            return(
                <>
                <h3>Inventory</h3>
                <TextField fullWidth  name="pdName" onChange={changeEle} value={prod.pdName} label="Product Name" variant="filled" style={{margin: '2% 0'}}/>
                <TextField fullWidth  name="pdQty" onChange={changeEle} value={prod.pdQty}label="Product Qty" variant="filled" style={{margin: '2% 0'}}/>
                <TextField fullWidth  name="pdPrice" onChange={changeEle} value={prod.pdPrice} label="Product Price" variant="filled" style={{margin: '2% 0'}}/>
                </>
            );
        }
    }

    const comp4 = (data) => {
        if(data.prodImage === true){
            return(
                <>
                <h3>Product Image</h3>
                <div>
                    <Dropzone onDrop={handleDrop3}>
                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps({ className: "dropzone" })}>
                                <input {...getInputProps()} />
                                <Button fullWidth size="large" color="primary" variant="contained">Upload Image</Button>
                            </div>
                        )}
                    </Dropzone>
                </div>
                </>
            );
        }
    }

    const [openA, setOpenA] = React.useState(false);

    const handleClickOpenA = () => {
        setOpenA(true);
    };

    const handleCloseA = () => {
        setOpenA(false);
    };

    const [openB, setOpenB] = React.useState(false);

    const handleClickOpenB = () => {
        setOpenB(true);
    };

    const handleCloseB = () => {
        setOpenB(false);
    };

    const submitProd = () => {
        console.log(prod)
        const { id } = queryString.parse(window.location.search);
        prod.pdId = id
        axios.post(`${API_SERVICE}/api/v1/main/addmobileproduct`, prod).then(res=>{
            handleCloseA()
            setProd({
                pdId: "",
                pdName: "",
                pdQty: "",
                pdPrice: "",
                pdFile1: "",
                pdFile2: "",
                pdFile3: "",
                date: d
            })
            window.location = '/storepage'
        }).catch(err => {
            console.log("error");
        })
    }
    const submitProdRec = () => {
        const { id } = queryString.parse(window.location.search);
        prod.pdId = id
        axios.post(`${API_SERVICE}/api/v1/main/addmobileproduct`, prod).then(res=>{
            handleClickOpenB()
            setProd({
                pdId: "",
                pdName: "",
                pdQty: "",
                pdPrice: "",
                pdFile1: "",
                pdFile2: "",
                pdFile3: "",
                date: d
            })
        }).catch(err => {
            console.log("error");
        })
    }

    return (
        <>
            <div className="container mt-4">
                <h2 className="fw-bold">
                    Add Product
                </h2>
                
                <BarcodeScannerComponent
                    width="100%"
                    height="auto"
                    onUpdate={(err, result) => {
                    if (result) setData(result.text)
                    else console.log(err)
                    }}
                />

                {data.length === 0 ? <div>Loading...</div> : data.map(comp1)}
                {data.length === 0 ? <div></div> : data.map(comp2)}
                {data.length === 0 ? <div></div> : data.map(comp3)}
                {data.length === 0 ? <div></div> : data.map(comp4)}
                
                <br></br>
                
                <Button
                    className="mt-3"
                    style={{ height: '50px' ,marginTop: '10px'}}
                    type="button"
                    fullWidth
                    size="large"
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    onClick={submitProdRec}
                >
                    Next
                </Button>
                <br></br>
                <Button
                    className="mt-3"
                    style={{ height: '50px' ,marginTop: '10px'}}
                    type="button"
                    fullWidth
                    size="large"
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                >
                    Undo
                </Button>
            
                <Button
                    className="mt-3"
                    style={{ height: '50px' ,marginTop: '10px'}}
                    type="button"
                    fullWidth
                    size="large"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={handleClickOpenA}
                >
                    Exit
                </Button>
                <Dialog
                    open={openA}
                    onClose={handleCloseA}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Alert"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to exit ?
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleCloseA} color="primary">
                        No
                    </Button>
                    <Button onClick={submitProd} color="primary" autoFocus>
                        Yes
                    </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={openB}
                    onClose={handleCloseB}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Alert"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Submitted Successfully, Do you want to make more entries ?
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button href='/storepage' color="primary">
                        No
                    </Button>
                    <Button onClick={handleCloseB} color="primary" autoFocus>
                        Yes
                    </Button>
                    </DialogActions>
                </Dialog>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <Snackbar
                    anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                    }}
                    open={openS}
                    autoHideDuration={3500}
                    onClose={handleCloseS}
                    message={textS}
                    action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseS}>
                        <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                    }
                />          
            </div>

            <Footer />
        </>
    )
}

export default AddProduct
