import React from 'react';
import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import { useCallback, useState } from 'react';
import { Form, FormikProvider, useFormik } from 'formik';
// material
import { LoadingButton } from '@material-ui/lab';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import {
  Card,
  Grid,
  Chip,
  Stack,
  Button,
  Switch,
  TextField,
  Typography,
  Autocomplete,
  FormHelperText,
  FormControlLabel
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
// utils
import fakeRequest from '../../../utils/fakeRequest';
//
import { QuillEditor } from '../../editor';
import { UploadSingleFile } from '../../upload';
//
import BlogNewPostPreview from './BlogNewPostPreview';
// ----------------------------------------------------------------------

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


import { firestore, storage } from '../../../Firebase/index';

import { v4 as uuid4 } from 'uuid';

import axios from 'axios';

import { API_SERVICE } from 'src/components/URI';

const TAGS_OPTION = [
  'Toy Story 3',
  'Logan',
  'Full Metal Jacket',
  'Dangal',
  'The Sting',
  '2001: A Space Odyssey',
  "Singin' in the Rain",
  'Toy Story',
  'Bicycle Thieves',
  'The Kid',
  'Inglourious Basterds',
  'Snatch',
  '3 Idiots'
];

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

// ----------------------------------------------------------------------

export default function BlogNewPostForm() {

  const [file1,setFile1] = useState([])

  const [file2,setFile2] = useState([])

  const [data, setData] = useState({
    category: "Build a planogram",
    shelf: false,
    image: false,
    inventory: false,
    prodImage: false,
    title: "",
    categoryLocation: "",
    content: "",
    notes: "",
    file1: "",
    file2: ""
  })

  const changeEle = (event) => {
    const {name, value} = event.target;
    if(name === "category"){
      setData(prevValue => {
        return {
          ...prevValue,
          shelf: false,
          image: false,
          inventory: false,
          prodImage: false,
        }
      })
    }
    setData(prevValue => {
        return {
            ...prevValue,
            [name]: value
        }
    })
}

React.useEffect(() => {
  if (file1.length > 0) {
      onSubmit1();
  } else {
      console.log("N");
  }
}, [file1]);

React.useEffect(() => {
  if (file2.length > 0) {
      onSubmit2();
  } else {
      console.log("N");
  }
}, [file2]);

const handleDrop1 = async (acceptedFiles) => {
  setFile1(acceptedFiles.map(file => file));
}

const handleDrop2 = async (acceptedFiles) => {
  setFile2(acceptedFiles.map(file => file));
}

const onSubmit1 = () => {
  if (file1.length > 0) {
      file1.forEach(file => {
          const timeStamp = Date.now();
          var uniquetwoKey = uuid4();
          uniquetwoKey = uniquetwoKey + timeStamp;
          const uploadTask = storage.ref(`temp/${uniquetwoKey}/${file.name}`).put(file);
          uploadTask.on('state_changed', (snapshot) => {
              const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
              setOpenS(true);
              setTextS(`Uploading ${progress} %`);
          },
          (error) => {
              console.log(error)
          },
          async () => {
              // When the Storage gets Completed
              const filePath = await uploadTask.snapshot.ref.getDownloadURL();
              setTextS('File Uploaded');
              setData(prevValue => {return {...prevValue,"file1": filePath}})
          });
      })
  } else {
      console.log('No File Selected Yet');
  }
}

const onSubmit2 = () => {
  if (file2.length > 0) {
      file2.forEach(file => {
          const timeStamp = Date.now();
          var uniquetwoKey = uuid4();
          uniquetwoKey = uniquetwoKey + timeStamp;
          const uploadTask = storage.ref(`temp/${uniquetwoKey}/${file.name}`).put(file);
          uploadTask.on('state_changed', (snapshot) => {
              const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
              setOpenS(true);
              setTextS(`Uploading ${progress} %`);
          },
          (error) => {
              console.log(error)
          },
          async () => {
              // When the Storage gets Completed
              const filePath = await uploadTask.snapshot.ref.getDownloadURL();
              setTextS('File Uploaded');
              setData(prevValue => {return {...prevValue,"file2": filePath}})
          });
      })
  } else {
      console.log('No File Selected Yet');
  }
}
  
  // const [projectcategory, setprojectcategory] = useState('Build a planogram');

  const [state, setState] = React.useState({
    inventoryCheck: false,
    imageUpload: false,
    osa: false,
    imagePlanogram: false,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const { enqueueSnackbar } = useSnackbar();
  const [open, setOpen] = useState(false);

  const handleOpenPreview = () => {
    setOpen(true);
  };

  const handleClosePreview = () => {
    setOpen(false);
  };

  const NewBlogSchema = Yup.object().shape({
    title: Yup.string().required('Title is required'),
    description: Yup.string().required('Description is required'),
    content: Yup.string().min(1000).required('Content is required'),
    cover: Yup.mixed().required('Cover is required')
  });

  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      content: '',
      cover: null,
      tags: ['Logan'],
      publish: true,
      comments: true,
      metaTitle: '',
      metaDescription: '',
      metaKeywords: ['Logan']
    },
    // validationSchema: NewBlogSchema,
    // onSubmit: async (values, { setSubmitting, resetForm }) => {
    //   try {
    //     await fakeRequest(500);
    //     resetForm();
    //     handleClosePreview();
    //     setSubmitting(false);
    //     enqueueSnackbar('Post success', { variant: 'success' });
    //   } catch (error) {
    //     console.error(error);
    //     setSubmitting(false);
    //   }
    // }
  });

  const { errors, values, touched, handleSubmit, isSubmitting, setFieldValue, getFieldProps } = formik;

  const handleDrop = useCallback(
    (acceptedFiles) => {
      const file = acceptedFiles[0];
      if (file) {
        setFieldValue('cover', {
          ...file,
          preview: URL.createObjectURL(file)
        });
      }
    },
    [setFieldValue]
  );

  const projSubmit = () => {
     axios.post(`${API_SERVICE}/api/v1/main/addproject`, data).then((res) => {
      setOpenS(true)
      setTextS('Project Submited')
      setData({
        category: "Build a planogram",
        shelf: false,
        image: false,
        inventory: false,
        prodImage: false,
        title: "",
        categoryLocation: "",
        content: "",
        notes: "",
        file1: "",
        file2: ""
      })
     }).catch((err) => {
       console.log('error');
     })
  }

  const [openS, setOpenS] = React.useState(false);
  const [textS, setTextS] = React.useState("");

  const handleClickS = () => {
    setOpenS(true);
  };

  const handleCloseS = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenS(false);
  };

  return (
    <>
      <FormikProvider value={formik}>
        <Form noValidate autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Card sx={{ p: 3 }}>
                <Stack spacing={3}>
                  
                  <InputLabel id="demo-simple-select-label">Project Category</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    fullWidth
                    name="category"
                    onChange={changeEle}
                    value={data.category}
                    // name="category" value={data.category} onChange={changeEle} 
                  >
                    <MenuItem value="Build a planogram">Build a planogram</MenuItem>
                    <MenuItem value="Planogram compliance">Planogram compliance</MenuItem>
                    <MenuItem value="Signage Compliance">Signage Compliance</MenuItem>
                    <MenuItem value="Stock check">Stock check</MenuItem>
                  </Select>

                  {
                    data.category === "Planogram compliance" || data.category === "Build a planogram" ? (
                      <>
                        <FormControlLabel
                          control={
                            <Switch
                              checked={data.shelf}
                              onChange={(event) => setData(prevValue => {return {...prevValue,"shelf": event.target.checked}})}
                              color="primary" 
                            />
                          }
                          label="On Shelf Avalability"
                        />

                        <FormControlLabel
                          control={
                            <Switch
                              checked={data.image}
                              onChange={(event) => setData(prevValue => {return {...prevValue,"image": event.target.checked}})}
                              color="primary"
                            />
                          }
                          label="Image of Planogram"
                        />
                      </>
                    ) : null
                  }

                  {
                    data.category === "Stock check" ? (
                      <>
                        <FormControlLabel
                          control={
                            <Switch
                              checked={data.inventory}
                              onChange={(event) => setData(prevValue => {return {...prevValue,"inventory": event.target.checked}})}
                              color="primary"
                            />
                          }
                          label="Inventory Check"
                        />

                        <FormControlLabel
                          control={
                            <Switch
                              checked={data.prodImage}
                              onChange={(event) => setData(prevValue => {return {...prevValue,"prodImage": event.target.checked}})}
                              color="primary"
                            />
                          }
                          label="Upload Product Image"
                        />
                      </>
                    ) : null
                  }

                  
                  
                  <TextField
                    fullWidth
                    label="Title"
                    {...getFieldProps('title')}
                    //error={Boolean(touched.title && errors.title)}
                    //helperText={touched.title && errors.title}
                    name="title" value={data.title} onChange={changeEle} 

                  />

                  <TextField
                    fullWidth
                    label="Category/Location"
                    {...getFieldProps('title')}
                    //error={Boolean(touched.title && errors.title)}
                    //helperText={touched.title && errors.title}
                    name="categoryLocation" value={data.categoryLocation} onChange={changeEle} 
                  />

                  <div>
                    <LabelStyle>Content</LabelStyle>
                    <TextField
                    fullWidthsx={{ p: 3 }}
                    multiline
                    minRows={3}
                    maxRows={5}
                    label="Content"
                    {...getFieldProps('description')}
                    error={Boolean(touched.description && errors.description)}
                    helperText={touched.description && errors.description}
                    fullWidth
                    name="content" value={data.content} onChange={changeEle} 
                  />
                  </div>

                  <div>
                    <LabelStyle>Note</LabelStyle>
                    <TextField
                    fullWidthsx={{ p: 3 }}
                    multiline
                    minRows={3}
                    maxRows={5}
                    label="Note"
                    {...getFieldProps('description')}
                    error={Boolean(touched.description && errors.description)}
                    helperText={touched.description && errors.description}
                    fullWidth
                    name="notes" value={data.notes} onChange={changeEle} 
                  />
                  </div>

                  
                    <div>
                      <LabelStyle>Upload Image of Signage</LabelStyle>
                      <UploadSingleFile
                        maxSize={3145728}
                        accept="image/*"
                        file={values.cover}
                        onDrop={handleDrop1}
                        error={Boolean(touched.cover && errors.cover)}
                      />
                      {touched.cover && errors.cover && (
                        <FormHelperText error sx={{ px: 2 }}>
                          {touched.cover && errors.cover}
                        </FormHelperText>
                      )}
                    </div>

                    <div>
                      <LabelStyle>Upload Planogram in form of CSV</LabelStyle>
                        <UploadSingleFile
                          maxSize={3145728}
                          accept="image/*"
                          file={values.cover}
                          onDrop={handleDrop2}
                          error={Boolean(touched.cover && errors.cover)}
                        />
                        {touched.cover && errors.cover && (
                          <FormHelperText error sx={{ px: 2 }}>
                            {touched.cover && errors.cover}
                          </FormHelperText>
                        )}
                      </div>
                </Stack>
                <br />
                <Button fullWidth type="button" variant="contained" size="large" onClick={projSubmit}>
                  Post Project
                </Button>
              </Card>
            </Grid>
          </Grid>
        </Form>
      </FormikProvider>

      <BlogNewPostPreview formik={formik} openPreview={open} onClosePreview={handleClosePreview} />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openS}
        autoHideDuration={5000}
        onClose={handleCloseS}
        message={textS}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseS}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </>
  );
}
