// material
import { Container } from '@material-ui/core';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { BlogNewPostForm } from '../../components/_dashboard/blog';

// ----------------------------------------------------------------------

export default function BlogNewPost() {
  return (
    <Page title="My Retailguru">
      <Container>
        <HeaderBreadcrumbs
          heading="Create a new Project"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
          ]}
        />

        <BlogNewPostForm />
      </Container>
    </Page>
  );
}
