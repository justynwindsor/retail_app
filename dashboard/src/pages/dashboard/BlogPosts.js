import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { Button } from '@material-ui/core';
import axios from 'axios';

import { API_SERVICE } from 'src/components/URI';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function BlogPosts() {
  const classes = useStyles();

  const [data, setData] = useState([])

  useEffect(()=>{
    axios.get(`${API_SERVICE}/api/v1/main/getallprojects`).then(res=>{
      setData(res.data.data)
    }).catch(err => {
      console.log("error");
    })
  }, [])


  return (
    <>
    <h1 style={{margin: "0 20px"}}>All Projects</h1>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell><h3>Project Title</h3></TableCell>
            <TableCell align="right"><h3>Details&nbsp;</h3></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.length===0?<div style={{margin: "20px"}}>Loading...</div>: data.map((row) => (
            <TableRow key={row._id}>
              <TableCell component="th" scope="row">
                <h3>{row.title}</h3>
              </TableCell>
              <TableCell align="right">
                <Button  href={`/dashboard/blog/project?id=${row._id}`} variant="contained" color="primary" >View More</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
  );
}
