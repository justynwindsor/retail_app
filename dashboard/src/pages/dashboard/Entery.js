import React, {useState, useEffect} from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { Button } from '@material-ui/core';
import axios from 'axios';
import queryString from 'query-string';

import { API_SERVICE } from 'src/components/URI';

const useStyles = makeStyles({
  table: {
    minWidth: 550,
    maxWidth: 750,
  },
});

export default function BlogPosts() {
  const classes = useStyles();

  const [data, setData] = useState([])

  useEffect(()=>{
    const { id } = queryString.parse(window.location.search);
    axios.get(`${API_SERVICE}/api/v1/main/getmobileproductbyidreal/${id}`).then(res=>{
        setData(res.data)
    }).catch(err => {
        console.log("error");
    })
  }, [])

  const table = (proj) => {
    return(
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableBody>

            {proj.pdName === "" ? <div></div> :(
            <>
            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Product Name</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.pdName}</h3>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Product Quantity</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.pdQty}</h3>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Product Price</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.pdPrice}</h3>
              </TableCell>
            </TableRow>
            </>
            )}

            {proj.pdFile1 === "" ? <div></div> :(
            <TableRow>
                <TableCell>
                <h3>Shelf Availability</h3>
                <Button href={proj.pdFile1} variant="contained" color="secondary">Download</Button>
                </TableCell>
            </TableRow>
            )}

            {proj.pdFile2 === "" ? <div></div> :(
            <TableRow>
                <TableCell>
                <h3>Planogram</h3>
                <Button href={proj.pdFile2}  variant="contained" color="secondary">Download</Button>
                </TableCell>
            </TableRow>
            )}

            {proj.pdFile3 === "" ? <div></div> :(
            <TableRow>
                <TableCell>
                <h3>Product Image</h3>
                <Button href={proj.pdFile3}  variant="contained" color="secondary">Download</Button>
                </TableCell>
            </TableRow>
            )}

        </TableBody>
      </Table>
    </TableContainer>
    );
  }

  return (
    <>
        <h1>Entery Details</h1>
        {data.length === 0 ? <div style ={{marginLeft: '24px'}}>Loading...</div>: data.map(table)}
    </>
  );
}
