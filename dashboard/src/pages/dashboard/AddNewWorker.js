// material
import { Container } from '@material-ui/core';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import AddNewWorkerProject from '../../components/_dashboard/blog/AddNewWorker';

// ----------------------------------------------------------------------

export default function AddNewWorker() {
  return (
    <Page title="My Retailguru">
      <Container>
        <HeaderBreadcrumbs
          heading="Add Workers"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
          ]}
        />

        <AddNewWorkerProject />
      </Container>
    </Page>
  );
}
