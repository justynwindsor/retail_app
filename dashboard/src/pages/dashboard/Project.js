import React, {useState, useEffect} from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { Button } from '@material-ui/core';
import axios from 'axios';
import queryString from 'query-string';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import { API_SERVICE } from 'src/components/URI';

const useStyles = makeStyles({
  table: {
    minWidth: 550,
  },
});

export default function BlogPosts() {
  const classes = useStyles();

  const [data, setData] = useState([])
  const [data2, setData2] = useState([])

  useEffect(()=>{
    const { id } = queryString.parse(window.location.search);
    axios.get(`${API_SERVICE}/api/v1/main/getproject/${id}`).then(res=>{
        setData(res.data)
    }).catch(err => {
        console.log("error");
    })
  }, [])

  const deleteProj = () => {
    const { id } = queryString.parse(window.location.search);
    axios.get(`${API_SERVICE}/api/v1/main/delproject/${id}`).then(res=>{
        console.log("deleted")
        window.location = "posts"
    }).catch(err => {
        console.log("error");
    })
  }

  useEffect(()=>{
    const { id } = queryString.parse(window.location.search);
    console.log(id);
    axios.get(`${API_SERVICE}/api/v1/main/getmobileproductbyid/${id}`).then(res=>{
        setData2(res.data)
        console.log(res)
    }).catch(err => {
        console.log("error");
    })
  }, [])

  const table = (proj) => {
    return(
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Category</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.category}</h3>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>On Shelt Availabilty</h3>
              </TableCell>
              <TableCell align="right">
                {proj.shelf === true ? <h3>True</h3> : <h3>False</h3>}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Image of Planogram</h3>
              </TableCell>
              <TableCell align="right">
                {proj.image === true ? <h3>True</h3> : <h3>False</h3>}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Inventory Check</h3>
              </TableCell>
              <TableCell align="right">
                {proj.inventory === true ? <h3>True</h3> : <h3>False</h3>}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Upload Product Image</h3>
              </TableCell>
              <TableCell align="right">
                {proj.prodImage === true ? <h3>True</h3> : <h3>False</h3>}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Title</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.title}</h3>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Category/Location</h3>
              </TableCell>
              <TableCell align="right">
                <h3>{proj.categoryLocation}</h3>
              </TableCell>
            </TableRow>

            <TableRow>
                <div style={{marginLeft: "24px"}}>
                <h4>Content</h4>
                <p>{proj.content}</p>
                </div>
            </TableRow>
            <br></br>
            <TableRow>
                <div style={{marginLeft: "24px"}}>
                    <h4>notes</h4>
                    <p>{proj.notes}</p>
                </div>  
            </TableRow>
            <br></br>
            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Image of Signage</h3>
              </TableCell>
              <TableCell align="right">
                <Button href={proj.file1} variant="contained" color="secondary">Click Here</Button>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                <h3>Planogram CSV</h3>
              </TableCell>
              <TableCell align="right">
                <Button href={proj.file2} variant="contained" color="secondary">Click Here</Button>
              </TableCell>
            </TableRow>

        </TableBody>
      </Table>
    </TableContainer>
    );
  }

  const entery = (ent, idx) => {
      var date = new Date(ent.date).toDateString()
      console.log(date)
      return(
        <>
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
            <TableBody>
            <TableRow style={{padding: '0 10px'}}>
            <div>
            <Card variant="outlined">
                <CardContent>
                    <h3> Entery {idx+1} </h3>
                    <h3> Created on: {date}</h3>
                </CardContent>
                <CardActions>
                    <Button href={`entery?id=${ent._id}`} style={{margin: '0 12px'}} size="large">See More</Button>
                </CardActions>
            </Card>
            <br></br>
            </div>
            </TableRow>
            </TableBody>
            </Table>
       </TableContainer>
       </>
      )
  }

  return (
    <>
        <div style={{textAlign: 'right'}}>
          <Button style={{marginRight: '24px'}}variant="contained" color="primary" onClick={deleteProj}>
            Delete
          </Button>
        </div>
        <h1 style ={{marginLeft: '24px'}}>Project Details</h1>
        {data.length === 0 ? <div>Loading</div> : data.map(table)}
        {data2.length === 0 ? <div></div>: data2.map(entery)}
    </>
  );
}
